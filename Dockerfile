FROM php:7.3-apache

ENV PORT 8080

ENV APACHE_DOCUMENT_ROOT=/var/www/html/public

RUN apt-get update && apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    && docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install -j$(nproc) pdo_mysql

RUN docker-php-ext-install exif

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN a2enmod rewrite

# Install R
RUN apt-get update && apt-get install -y r-base

# Install R packages
RUN R -e "install.packages('jsonlite',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('dplyr',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('caret',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('knitr',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('Information',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('bigequery',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('stringr',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('ppcor',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('pROC',dependencies=TRUE, repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('OneR',dependencies=TRUE, repos='http://cran.rstudio.com/')"

# Move startup script to docker
COPY start.sh /usr/local/bin/start
RUN chmod u+x /usr/local/bin/start

WORKDIR /var/www/html/

# Start docker
CMD ["/usr/local/bin/start"]
